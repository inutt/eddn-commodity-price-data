# Elite Dangerous average commodity prices

A repository containing the average price of commodities in the [Elite
Dangerous](https://www.elitedangerous.com/) galaxy, for use with my [EDMC cargo
manifest](https://gitlab.com/inutt/edmc-cargo-manifest) plugin. Only prices
seen in the last 30 days are included in the average.

You probably won't need to interact with this repository directly as checking
for updates will usually be handled by the plugin. The id of the latest commit
can be found with:
```bash
curl --no-progress-meter \
    'https://gitlab.com/api/v4/projects/inutt%2Feddn-commodity-price-data/repository/commits' \
    | jq '.[0].id'
```

All data is obtained via the community provided [Elite Dangerous Data
Network](https://github.com/EDCD/EDDN), using a custom [commodity price
listener](https://gitlab.com/inutt/eddn-listener).
